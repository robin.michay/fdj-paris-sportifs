# FDJ - Paris sportifs

## Versions

Docker: 2.1.0.5
Docker Engine: 19.03.5
Node: 12.x LTS
Angular: 8

## Structure

Here's the project's structure :

```
| database/ # Contains MongoDB *.bson & *.metadata.json files
| fdj-api/ # RESTful API with Express.js
| fdj-client/ # Angular with SSR (Express)
| docker-compose.yml # Compose managing all containers
| Makefile # Makefile to execute command in compose
```

## Installation

You can run the whole project with one simple command :

    make start


Usually, I don't push database files to github, but current datas aren't sensitive and are minimal and it makes it easier to run the project.

In another terminal, just fill the database with datas inside `database` folder :

    make db/fill

You can now navigate to http://localhost:3000/apidocs/ to see the Swagger API or go to http://localhost:4000/ to use the app.

## API

The API was handmade without using common libraries as `mongoose` to make it as simple as possible.
No model, no logic, it just retrieves, finds and inserts data.

If the client wants to access to the API, a token (expired after 6 hours) is needed.

The client must first get a token from http://localhost:3000/token, then store the token to finally put in in Headers['Authorization'] (Bearer <TOKEN>).

TypeScript was set to harmonize the code between the App & the API.

The documentation can be found in `fdj-api/src/swagger.json`;

## App

The application was made using Angular 8. It uses Angular University & Express to add the Server-Side Rendering (for the crawlers and the first page time response).

The UI Toolkit `Material Angular` was also added to design pages in less time.

See more here: https://material.angular.io/

## Database

The database used in this project is MongoDB. Why MongoDB ? Because it's fast to implement. But the current database structure needs to join Tables (league > team, team > players), so I guess PostgreSQL would have been a better choice for the production stage.

## Docker

Docker-compose is a tool for managing different containers. With the client-side, and the server-side, it's the perfect tool for this project.

Plus, it's easy to use and it removes the "incompatibility" part of most packages.
Moreover, it also facilitates the setup of the project (it's never easy to change packages' versions).

This will make it easier to configure the project for a production stage.

Easy to deploy with Cloud Services.

## Tests

As you can see, there's no testing part. I couldn't make all configurations in time, but if I would, I'd like adding some functional tests for the API and some End-to-End tests for the client, without speaking about the CI / CD through gitlab-ci.

Of course you can still run the defaults tests created by Angular-CLI, but it's meaningless.

## Duration

This project was made in approximatively 10 hours.