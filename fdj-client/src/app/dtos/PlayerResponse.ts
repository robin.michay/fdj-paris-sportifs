export interface PlayerResponse {
  success: boolean;
  data: {
    _id: string;
    name: string;
    position: string;
    thumbnail: string;
    signin: {
      amount: number;
      currency: string;
    };
    born: string;
  }[];
};
