export interface TeamResponse {
  success: boolean;
  data: {
    _id: string;
    name: string;
    thumbnail: string;
    players: string[];
  }[];
};

export interface SingleTeamResponse {
  success: boolean;
  data: {
    _id: string;
    name: string;
    thumbnail: string;
    players: string[];
  };
};
