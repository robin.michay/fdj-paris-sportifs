export interface LeagueResponse {
  success: boolean;
  data: {
    _id: string;
    name: string;
    sport: string;
    teams: string[];
  }[];
};

export interface SingleLeagueResponse {
  success: boolean;
  data: {
    _id: string;
    name: string;
    sport: string;
    teams: string[];
  };
};
