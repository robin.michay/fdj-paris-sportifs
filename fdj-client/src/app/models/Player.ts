export enum Currency {
  GPP = 'gpp',
  EUR = 'eur'
};

export class Player {
  public _id: string;
  public name: string;
  public position: string;
  public thumbnail: string;
  public signin: {
    amount: number;
    currency: Currency;
  };
  public born: Date;
};
