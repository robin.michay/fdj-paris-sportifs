import { Team } from './Team';

export enum Sport {
  Soccer = 'soccer',
  Basketball = 'basketball'
}

export class League {
  public _id: string;
  public name: string;
  public sport: Sport;
  public teams: Team["_id"][];
};
