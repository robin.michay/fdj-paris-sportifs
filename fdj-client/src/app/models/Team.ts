import { Player } from './Player';

export class Team {
  public _id: string;
  public name: string;
  public thumbnail: string;
  public players: Player["_id"][];
};
