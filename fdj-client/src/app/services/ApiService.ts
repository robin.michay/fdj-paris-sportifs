import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders, HttpResponse, HttpEventType } from '@angular/common/http';

const API_URI = 'http://localhost:3000';

interface Authorization {
  token: string;
  expiresAt: number;
}

@Injectable()
export class ApiService {
  private authorization: Authorization;

  constructor(private http: HttpClient) {}

  private async getToken() {
    const req = new HttpRequest('GET', `${API_URI}/token`, {}, {
      reportProgress: true
    });
    return this.http.request<Authorization>(req).toPromise();
  }

  public async request(method: 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE', url: string, data?: {}) {
    if (!this.authorization || this.authorization.expiresAt <= Date.now()) {
      const response = await this.getToken();
      if (response.type === HttpEventType.Response && response.body) {
        this.authorization = response.body;
      }
    }

    const req = new HttpRequest(method, `${API_URI}/${url}`, data, {
      reportProgress: true,
      headers: new HttpHeaders({
        'Authorization': 'Bearer ' + this.authorization.token
      })
    });
    return this.http.request(req).toPromise();
  }

  public get(url: string, query: { [s: string]: string }) {
    const keys = Object.keys(query);
    let fullUrl: string;
    if (keys.length > 0) {
      fullUrl = `${url}?`;
      for (const key of keys) {
        fullUrl += `${key}=${query[key]}&`;
      }
    } else {
      fullUrl = url;
    }
    return this.request('GET', fullUrl);
  }

  public post(url: string, data: { [s:string]: any }) {
    this.request('GET', url, data);
  }

  public put(url: string, data: { [s:string]: any }) {
    this.request('PUT', url, data);
  }

  public patch(url: string, data: { [s:string]: any }) {
    this.request('PATCH', url, data);
  }

  public delete(url: string, data: { [s:string]: any }) {
    this.request('DELETE', url, data);
  }
}
