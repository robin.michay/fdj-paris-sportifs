import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpEventType } from '@angular/common/http';
import { ApiService } from 'src/app/services/ApiService';
import { Team } from 'src/app/models/Team';
import { TeamResponse } from 'src/app/dtos/TeamResponse';
import { League } from 'src/app/models/League';
import { ActivatedRoute } from '@angular/router';
import { SingleLeagueResponse } from 'src/app/dtos/LeagueResponse';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
  public id: string;
  public league: League;
  public teams: Team[];
  public searchFormControl = new FormControl('', []);

  constructor(private apiService: ApiService, private route: ActivatedRoute) {
    this.route.params.subscribe(
      (params) => {
        this.id = params.id
        this.doFetch();
      }
    );
  }

  ngOnInit() {
  }

  async doFetch() {
    const leagueResponse = await this.apiService.get(`api/leagues/${this.id}`, {});
    if (leagueResponse.type === HttpEventType.Response && leagueResponse.body) {
      this.league = Object.assign(
        new League(),
        (<SingleLeagueResponse>leagueResponse.body).data
      );
    }

    if (this.league.teams.length) {
      const query = {};
      for (let i = 0; i < this.league.teams.length; i++) {
        query[`_id[${i}]`] = this.league.teams[i];
      }

      this.fetch(query);
    }
  }

  async fetch(query: { [s:string]: string }) {
    const teamsResponse = await this.apiService.get('api/teams', query);
    if (teamsResponse.type === HttpEventType.Response && teamsResponse.body) {
      this.teams = (<TeamResponse>teamsResponse.body).data.map(b => {
        const team = Object.assign(new Team(), b);
        return team;
      });
    }
  }
}
