import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpEventType } from '@angular/common/http';
import { ApiService } from 'src/app/services/ApiService';
import { League } from 'src/app/models/League';
import { LeagueResponse } from 'src/app/dtos/LeagueResponse';

@Component({
  selector: 'app-leagues',
  templateUrl: './leagues.component.html',
  styleUrls: ['./leagues.component.scss']
})
export class LeaguesComponent implements OnInit {
  public leagues: League[];
  public searchFormControl = new FormControl('', []);

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.doFetch();
  }

  doFetch() {
    this.fetch({
      name: this.searchFormControl.value.toLowerCase()
    });
  }

  async fetch(query: { [s: string]: string }) {
    const response = await this.apiService.get('api/leagues', query);
    if (response.type === HttpEventType.Response && response.body) {
      this.leagues = (<LeagueResponse>response.body).data.map(b => {
        const league = Object.assign(new League(), b);
        return league;
      });
    }
  }
}
