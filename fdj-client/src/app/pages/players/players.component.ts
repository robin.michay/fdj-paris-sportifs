import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpEventType } from '@angular/common/http';
import { ApiService } from 'src/app/services/ApiService';
import { Player } from 'src/app/models/Player';
import { PlayerResponse } from 'src/app/dtos/PlayerResponse';
import { Team } from 'src/app/models/Team';
import { ActivatedRoute } from '@angular/router';
import { SingleTeamResponse } from 'src/app/dtos/TeamResponse';

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html',
  styleUrls: ['./players.component.scss']
})
export class PlayersComponent implements OnInit {
  public id: string;
  public team: Team;
  public players: Player[];
  public searchFormControl = new FormControl('', []);

  constructor(private apiService: ApiService, private route: ActivatedRoute) {
    this.route.params.subscribe(
      (params) => {
        this.id = params.id
        this.doFetch();
      }
    );
  }

  ngOnInit() {
  }

  async doFetch() {
    const teamResponse = await this.apiService.get(`api/teams/${this.id}`, {});
    if (teamResponse.type === HttpEventType.Response && teamResponse.body) {
      this.team = Object.assign(
        new Team(),
        (<SingleTeamResponse>teamResponse.body).data
      );
    }

    if (this.team.players.length) {
      const query = {};
      for (let i = 0; i < this.team.players.length; i++) {
        query[`_id[${i}]`] = this.team.players[i];
      }

      this.fetch(query);
    }
  }

  async fetch(query: { [s:string]: string }) {
    const playersResponse = await this.apiService.get('api/players', query);
    if (playersResponse.type === HttpEventType.Response && playersResponse.body) {
      this.players = (<PlayerResponse>playersResponse.body).data.map(b => {
        const player: Player = Object.assign(new Player(), b);
        player.born = new Date(b.born);
        return player;
      });
    }
  }
}
