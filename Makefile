start:
	docker-compose up client

server:
	docker-compose up server

db/fill:
	docker-compose exec -T database mongorestore -d fdj-dev /database/

db/connect:
	docker-compose exec -T database mongo
