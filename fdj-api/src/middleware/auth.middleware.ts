import { Request } from 'express';
import jwt from 'express-jwt';

const getTokenFromHeader = (req: Request) => {
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1];
  } else if (req.query && req.query.token) {
    return req.query.token;
  }
  return null;
}

export default jwt({
  secret: 'secret-jwt-token-fdj-technical-test',
  getToken: getTokenFromHeader,
}).unless({
  path: ['/token', '/apidocs/']
});
