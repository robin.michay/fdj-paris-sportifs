'use strict';
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import morgan from 'morgan';
import swaggerUi from 'swagger-ui-express';
import { MongoClient } from 'mongodb';

import { DATABASE, MONGO_URI } from './config';
import DatabaseRoutes from './api/database';
import AuthMiddleware from './middleware/auth.middleware';
import AuthService from './services/auth.service';

MongoClient.connect(MONGO_URI, (err, client) => {
  if (err) {
    console.log('Cannot connect to database: ', err.message);
  } else {
    // Constants
    const PORT = +process.env.port || 3000;
    const HOST = '0.0.0.0';

    // App
    const app = express();
    const db = client.db(DATABASE);

    // Enhance API's security
    app.use(helmet());

    // Parse JSON bodies into JS objects
    app.use(bodyParser.json());

    // Enabling CORS
    app.use(cors());

    // Log HTTP requests
    app.use(morgan('combined'));

    const swaggerDocument = require('./swagger.json');
    app.use('/apidocs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

    // Force JWT Token for all routes except /token
    app.use(AuthMiddleware);

    DatabaseRoutes(app, db);

    app.get('/token', (req, res) => {
      res.contentType('application/json');
      try {
        const token = AuthService.generateToken();
        res.status(200).json(token);
      } catch (e) {
        res.status(500).json({
          success: false,
          message: e.message
        });
      }
    });

    app.listen(PORT, HOST, () => {
      console.log(`Running on http://${HOST}:${PORT}`);
    });
  }
});

