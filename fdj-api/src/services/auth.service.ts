import jwt from 'jsonwebtoken';

export default class AuthService {
  public static generateToken() {

    const data = {
        // You can pass additional data here
    };
    const signature = 'secret-jwt-token-fdj-technical-test';
    const expiration = '6h';

    return {
      token: jwt.sign({ data }, signature, { expiresIn: expiration }),
      expiresAt: Date.now() + 3600000
    };
  }
}
