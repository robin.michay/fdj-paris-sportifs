'use strict';

const isProduction = process.env.NODE_ENV === 'production';

export const DATABASE = isProduction ? 'fdj' : 'fdj-dev';
export const MONGO_HOST = 'database';
export const MONGO_PORT = 27017;
export const MONGO_URI = `mongodb://${MONGO_HOST}:${MONGO_PORT}/${DATABASE}`;
