import { Request, Response, Express } from 'express';
import { Db, ObjectID } from 'mongodb';

class Database {
  private db: Db;

  constructor(db: Db) {
    this.db = db;
  }

  private objectId(id: string) {
    let dbObjectID = new ObjectID(id);
    if (id.length === 24 && parseInt(dbObjectID.getTimestamp().toISOString().slice(0, 4), 10) >= 2010) {
      return dbObjectID;
    }
    return id;
  };

  // Function callback
  private fn(req: Request, res: Response) {
    return (err: Error, results: any) => {
      res.contentType('application/json');
      let response: {
        success: boolean;
        message?: string;
        data?: any;
      };
      let status: number;
      if (err) {
        status = 500;
        if (err.message) {
          response = {
            success: false,
            message: err.message
          };
        } else {
          response = {
            success: false,
            message: JSON.stringify(err)
          };
        }
      } else {
        status = 200;
        if (results && results.result && results.result.ok == 1 && results.ops) {
          response = {
            success: true,
            data: results.ops[0]
          };
        } else {
          response = {
            success: true,
            data: results
          }
        }
      }

      res.status(status).json(response);
    };
  };

  /* Routes */

  // Query
  // app.get('/api/:collection')
  public query(req: Request, res: Response) {
    let item: string,
      sort = {},
      qw = {};
    const skip = parseInt(req.query.skip, 10) || 0;
    for (item in req.query) {
      req.query[item] = (typeof + req.query[item] === 'number' && isFinite(req.query[item])) ?
        parseFloat(req.query[item]) :
        req.query[item];
      if (item != 'limit' && item != 'skip' && item != 'sort' && item != 'order' && req.query[item] != 'undefined' && req.query[item]) {
        if (typeof req.query[item] === 'string') {
          qw[item] = new RegExp(req.query[item], 'i');
        } else if (Array.isArray(req.query[item])) {
          if (item === '_id')
            req.query[item] = req.query[item].map(id => this.objectId(id));
          qw[item] = { $in: req.query[item] };
        }
      }
    }
    if (req.query.sort) {
      sort[req.query.sort] = (req.query.order === 'desc' || req.query.order === -1) ? -1 : 1;
    }
    if (req.query.limit) {
      this.db.collection(req.params.collection).find(qw).sort(sort).skip(skip).limit(req.query.limit).toArray(this.fn(req, res));
    } else {
      this.db.collection(req.params.collection).find(qw).sort(sort).skip(skip).toArray(this.fn(req, res));
    }
  };

  // Read
  // app.get('/api/:collection/:id')
  public async read(req: Request, res: Response) {
    this.db.collection(req.params.collection).findOne({
      _id: this.objectId(req.params.id)
    }, this.fn(req, res));
  }

  // Save
  // app.post('/api/:collection')
  // Mongo save is deprecated, we use replaceOne instead
  public async save(req: Request, res: Response) {
    if (req.body._id) {
      req.body._id = this.objectId(req.body._id);

      this.db.collection(req.params.collection).replaceOne({
          '_id': req.body._id
        },
        req.body, {
          upsert: true
        },
        this.fn(req, res)
      )
    } else {
      this.db.collection(req.params.collection).insertOne(req.body, this.fn(req, res));
    }
  }

  // Delete
  // app.del('/api/:collection/:id')
  public del(req: Request, res: Response) {
    this.db.collection(req.params.collection).remove({
      _id: this.objectId(req.params.id)
    }, this.fn(req, res));
  }
}

export default (app: Express, db: Db) => {
  const database = new Database(db);
  app.get('/api/:collection', database.query.bind(database));
  app.get('/api/:collection/:id', database.read.bind(database));
  app.delete('/api/:collection/:id', database.del.bind(database));
  app.post('/api/:collection/:id?', database.save.bind(database));
  app.patch('/api/:collection/:id?', database.save.bind(database));
  app.put('/api/:collection/:id?', database.save.bind(database));
};
